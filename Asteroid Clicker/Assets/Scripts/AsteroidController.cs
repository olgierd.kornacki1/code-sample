using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct AsteroidStatus
{
    public int hp;
    public Color color;
    public float size;
}

public class AsteroidController : MonoBehaviour
{
    public static AsteroidController Instance { get; private set; }

    public List<AsteroidStatus> status = new();

    public AsteroidPositionController positionController { get; private set; }
    public AsteroidPool asteroidPool { get; private set; }

    [SerializeField] private int initialAsteroidCount = 5;
    public int maxAsteroidCount = 10;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        asteroidPool = GetComponent<AsteroidPool>();
        positionController = GetComponent<AsteroidPositionController>();
    }

    private void Update()
    {
        if (GameController.Instance.isGameOn)
        {
            if (asteroidPool.UnactiveAsteroidCount > initialAsteroidCount)
            {
                int asteroidsToSpawn = Random.Range(1, maxAsteroidCount - (maxAsteroidCount - asteroidPool.UnactiveAsteroidCount) + 1);
                SpawnAsteroid(asteroidsToSpawn);
            }
        }
    }

    public void SpawnOnGameStart()
    {
        SpawnAsteroid(initialAsteroidCount);
    }

    private void SpawnAsteroid(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Asteroid asteroid = asteroidPool.Get();
            asteroid.transform.position = positionController.TakePosition(asteroid);
            asteroid.transform.eulerAngles = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
            asteroid.gameObject.SetActive(true);
        }
    }
}