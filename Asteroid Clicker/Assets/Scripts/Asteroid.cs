using System.Collections;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField] private int maxLifeTime = 3;

    private int _health = 3;
    private float _lifeTime;
    private Renderer _renderer;

    private bool _isChangingPosition;

    public int Health
    {
        get
        {
            return _health;
        }
        set
        {
            _health -= 1;
            if (_health == 0)
            {
                OnAsteroidDeath();
            }
            else if (_health > 0)
            {
                OnAsteroidDamage();
            }
        }
    }

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void OnEnable()
    {
        _health = 3;
        _lifeTime = Random.Range(1, maxLifeTime + 1);
        _renderer.material.color = AsteroidController.Instance.status[0].color;
        StartCoroutine(ChangeAsteroidSize(Vector3.one / 5, Vector3.one));
    }

    private void Update()
    {
        _lifeTime -= Time.unscaledDeltaTime;
        if (_lifeTime <= 0)
        {
            if (!_isChangingPosition)
            {
                AsteroidController.Instance.positionController.RemovePosition(this);
                StartCoroutine(ChangeAsteroidPosition(AsteroidController.Instance.positionController.TakePosition(this)));
            }
        }
    }

    public void ApplyDamage()
    {
        Health -= 1;
    }

    public void OnAsteroidDamage()
    {
        foreach (var status in AsteroidController.Instance.status)
        {
            if (status.hp == Health)
            {
                _renderer.material.color = status.color;
                StartCoroutine(ChangeAsteroidSize(Vector3.one * status.size));
                break;
            }
        }
    }

    public void OnAsteroidDeath()
    {
        AsteroidController.Instance.positionController.RemovePosition(this);
        AsteroidController.Instance.asteroidPool.ReturnToPool(this);
        GameController.Instance.Points += 1;
    }

    public IEnumerator ChangeAsteroidPosition(Vector3 targetPosition)
    {
        _isChangingPosition = true;
        float duration = 0.25f;
        float elapsedTime = 0f;

        Vector3 startingPosition = transform.position;

        while (elapsedTime < duration)
        {
            transform.position = Vector3.Lerp(startingPosition, targetPosition, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPosition;
        _isChangingPosition = false;
        _lifeTime = Random.Range(1, maxLifeTime + 1);
    }

    public IEnumerator ChangeAsteroidSize(Vector3 targetSize)
    {
        float duration = 0.25f;
        float elapsedTime = 0f;

        while (elapsedTime < duration)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, targetSize, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator ChangeAsteroidSize(Vector3 startSize, Vector3 targetSize)
    {
        float duration = 0.15f;
        float elapsedTime = 0f;
        transform.localScale = startSize;
        while (elapsedTime < duration)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, targetSize, elapsedTime / duration);
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        transform.localScale = targetSize;
    }
}