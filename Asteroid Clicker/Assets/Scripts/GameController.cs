using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    [SerializeField] private int maxPoints;
    [SerializeField] private int _points;

    [SerializeField] private TMPro.TextMeshProUGUI pointsText;

    public int Points
    {
        get { return _points; }
        set
        {
            _points = value;
            if (_points >= maxPoints)
            {
                EndGame();
            }

            pointsText.text = "Punkty: " + _points;
        }
    }

    public bool isGameOn;

    public GameObject endGameUI;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        endGameUI.SetActive(false);
        pointsText.gameObject.SetActive(true);
        Points = 0;
        AsteroidController.Instance.SpawnOnGameStart();
        isGameOn = true;
    }

    public void EndGame()
    {
        endGameUI.gameObject.SetActive(true);
        pointsText.gameObject.SetActive(false);
        AsteroidController.Instance.asteroidPool.DisablePool();
        isGameOn = false;
    }
}