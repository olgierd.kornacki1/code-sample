using UnityEngine;

public class CursorController : MonoBehaviour
{
    private ClickController clickController;

    private Camera mainCamera;

    private void Awake()
    {
        clickController = new ClickController();
        mainCamera = Camera.main;
    }

    private void OnEnable()
    {
        clickController.Enable();
    }

    private void OnDisable()
    {
        clickController.Disable();
    }

    private void Start()
    {
        clickController.Mouse.DoubleClick.performed += _ => DetectAsteroid();
    }

    private void DetectAsteroid()
    {
        Ray ray = mainCamera.ScreenPointToRay(clickController.Mouse.Position.ReadValue<Vector2>());

        if (Physics.Raycast(ray, out RaycastHit hitInfo))
        {
            if (hitInfo.collider != null)
            {
                Asteroid asteroid = hitInfo.collider.GetComponent<Asteroid>();
                asteroid.ApplyDamage();
            }
        }
    }
}