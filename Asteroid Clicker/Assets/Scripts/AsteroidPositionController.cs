using System.Collections.Generic;
using UnityEngine;

public class AsteroidPositionController : MonoBehaviour
{
    public int size;

    public List<AsteroidPosition> asteroidPositions = new();

    private void Start()
    {
        GeneratePositions();
    }

    public Vector3 TakePosition(Asteroid asteroid)
    {
        List<int> freePositionIndexes = new();

        for (int i = 0; i < asteroidPositions.Count; i++)
        {
            if (asteroidPositions[i].occupant == null)
            {
                freePositionIndexes.Add(i);
            }
        }

        if (freePositionIndexes.Count == 0)
        {
            Debug.LogWarning("No free positions available.");
            return Vector3.zero;
        }

        int randomIndex = freePositionIndexes[Random.Range(0, freePositionIndexes.Count)];
        var pos = asteroidPositions[randomIndex];
        pos.occupant = asteroid;
        return new Vector3(pos.cords.x, 0, pos.cords.y);
    }

    public void RemovePosition(Asteroid asteroid)
    {
        foreach (var pos in asteroidPositions)
        {
            if (pos.occupant == asteroid)
            {
                pos.occupant = null;
                break;
            }
        }
    }

    private void GeneratePositions()
    {
        for (int i = -size; i <= size; i += 2)
        {
            for (int j = -size; j <= size; j += 2)
            {
                AsteroidPosition position = new AsteroidPosition
                {
                    cords = new Vector2(i, j),
                    occupant = null
                };
                asteroidPositions.Add(position);
            }
        }
    }

    private void OnDrawGizmos()
    {
        foreach (var position in asteroidPositions)
        {
            if (position.occupant == null)
            {
                Gizmos.color = Color.green;
            }
            else
            {
                Gizmos.color = Color.red;
            }
            Gizmos.DrawWireCube(new Vector3(position.cords.x, 0, position.cords.y), Vector3.one);
        }
    }
}

[System.Serializable]
public class AsteroidPosition
{
    public Vector2 cords;
    public Asteroid occupant;
}