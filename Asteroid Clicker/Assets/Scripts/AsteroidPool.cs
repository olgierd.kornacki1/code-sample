using System.Collections.Generic;
using UnityEngine;

public class AsteroidPool : MonoBehaviour
{
    [SerializeField] private Asteroid asteroidPrefab;

    private Queue<Asteroid> asteroidsQueue = new();

    public int UnactiveAsteroidCount
    {
        get
        {
            return asteroidsQueue.Count;
        }
    }

    private void Start()
    {
        AddAsteroids(AsteroidController.Instance.maxAsteroidCount);
    }

    public Asteroid Get()
    {
        return asteroidsQueue.Dequeue();
    }

    private void AddAsteroids(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var asteroid = Instantiate(asteroidPrefab);
            asteroid.gameObject.SetActive(false);
            asteroidsQueue.Enqueue(asteroid);
        }
    }

    public void ReturnToPool(Asteroid asteroid)
    {
        asteroid.gameObject.SetActive(false);
        asteroidsQueue.Enqueue(asteroid);
    }

    public void DisablePool()
    {
        foreach (var asteroid in FindObjectsOfType<Asteroid>())
        {
            if (!asteroidsQueue.Contains(asteroid))
            {
                ReturnToPool(asteroid);
                AsteroidController.Instance.positionController.RemovePosition(asteroid);
            }
        }
    }
}